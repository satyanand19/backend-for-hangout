const app = require('express')();

const server = require('http').createServer(app);

const io = require('socket.io')(server,{

  cors: {
    origin: "*"
  }
});

io.on('connection',(socket) => {
  console.log('what is socket id',socket.id);
  console.log('Socket is active to be connected');

  socket.on('chat',(payload) => {
    console.log('what is payload',payload);
    io.emit('chat',payload);
  })
  socket.on('disconnect',() => {
    console.log('User get disconnected',socket.id)
  })
})

server.listen(5000,() => {
  console.log('server is listening on port 5000...');
})